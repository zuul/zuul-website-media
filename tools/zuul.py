# Zuul-ci pyramid logo in 3D
# SPDX short identifier: MIT

"""Render the zuul.glsl shader to a zuul.mp4 file at 25 fps"""

from subprocess import Popen
import numpy as np
from glumpy import app, gl, gloo
from PIL import Image

RES = [500, 500]
ROTATION = 2
FPS = 25

vertex = "attribute vec2 p; void main(void) {gl_Position = vec4(p, 0.0, 1.0);}"
fragment = open("zuul.glsl").read()
window = app.Window(width=RES[0], height=RES[1])
program = gloo.Program(vertex, fragment, count=4)
program['p'] = [(-1, -1), (-1, +1), (+1, -1), (+1, +1)]
program['iResolution'] = RES + [0]
gl.glEnable(gl.GL_BLEND)
gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
backend = app.__backend__
clock = app.__init__(backend=backend, framerate=FPS)
pixels = np.zeros((RES[0], RES[1] * 3), dtype=np.uint8)

for i in range(int(np.pi * ROTATION * FPS)):
    window.activate()
    window.clear()
    program["iTime"] = i / FPS
    program.draw(gl.GL_TRIANGLE_STRIP)
    gl.glReadPixels(
        0, 0, RES[0], RES[1], gl.GL_RGB, gl.GL_UNSIGNED_BYTE, pixels)
    image = Image.frombytes(
        "RGB", RES, np.ascontiguousarray(np.flip(pixels, 0)))
    image.save("%03d.png" % (i + 1), 'png')
    backend.process(clock.tick())

Popen(["ffmpeg", "-y", "-r", str(FPS), "-i", "%03d.png", "-pix_fmt", "yuv420p",
       "zuul.mp4"]).wait()
